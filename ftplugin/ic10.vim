" ftplugin/ic10.vim

" Highlight too long lines
let &colorcolumn=join(range(91,999),',')
" Highlight lines after line 128
match Error "\v%>128l"

let s:labelList = {}
let s:patternLabel ='\v^\w+\ze:'

function! s:hlLabels()
    call s:hlInstances(s:patternLabel, 'Label', s:labelList)
endfunction

function! s:hlInstances(pattern, hlgroup, trackerList)
    let l:buflen = line('$')
    let l:foundInstances = []

    " Loop all lines
    while l:buflen
        let l:buflen -= 1

        let l:curline = getline(l:buflen)
        let l:match = matchstr(l:curline, a:pattern, 0)

        if (empty(l:match))
            continue
        endif

        " Record the instance
        call add(l:foundInstances, l:match)

        " Highlight the instances, if not already highlighted (excluding comments)
        if ( ! has_key(a:trackerList, l:match))
            let a:trackerList[l:match] = matchadd( a:hlgroup, '^[^#]*\zs\<' . l:match . '\>' )
        endif
    endwhile

    " Remove highlighting of deleted instances
    if (! empty(a:trackerList))
        for [instance, matchID] in items(copy(a:trackerList))
            if ( index(l:foundInstances, instance) == -1) " instance not in foundInstances
                " Remove match
                call matchdelete(matchID)
                " Remove item from trackerList
                unlet a:trackerList[instance]
            endif
        endfor
    endif
endfunction

augroup ic10Labels
    autocmd!
    " Update when ic10 content is changed
    autocmd TextChanged *.ic10 call <SID>hlLabels()
    autocmd TextChangedI *.ic10 call <SID>hlLabels()
    " Update when opening a new ic10 file
    autocmd VimEnter,BufWinEnter *.ic10 call <SID>hlLabels()
augroup END

" Command to run label highlighting on demand
command! HlLabels :call <SID>hlLabels()

if exists("g:ic10_extended_syntax") && g:ic10_extended_syntax
    let s:aliasDeviceList = {}
    let s:aliasRegisterList = {}

    let s:patternAliasDevice = '\v^\s*alias\s+\zs\w+\ze\s+d[0-9]\s*(#.*)?$'
    let s:patternAliasRegister = '\v^\s*alias\s+\zs\w+\ze\s+r[0-9]{1,2}\s*(#.*)?$'

    function! s:hlDeviceAliases()
        call s:hlInstances(s:patternAliasDevice, 'String', s:aliasDeviceList)
    endfunction

    function! s:hlRegisterAliases()
        call s:hlInstances(s:patternAliasRegister, 'Typedef', s:aliasRegisterList)
    endfunction

    augroup ic10Extended
        autocmd!
        if ! exists("g:ic10_ext_alias_noreg") || g:ic10_ext_alias_noreg
            autocmd TextChanged *.ic10 call <SID>hlDeviceAliases()
            autocmd TextChangedI *.ic10 call <SID>hlDeviceAliases()
            autocmd VimEnter,BufWinEnter *.ic10 call <SID>hlDeviceAliases()
        endif

        if ! exists("g:ic10_ext_alias_nodev") || g:ic10_ext_alias_nodev
            autocmd TextChanged *.ic10 call <SID>hlRegisterAliases()
            autocmd TextChangedI *.ic10 call <SID>hlRegisterAliases()
            autocmd VimEnter,BufWinEnter *.ic10 call <SID>hlRegisterAliases()
        endif
    augroup END
endif
