Stationeers IC10 syntax highlighting for vim and neovim
=======================================================

A [vim][vim] and [neovim][neovim] syntax highlighting plugin for the IC10 MIPS
code used in the space survival game [Stationeers][stationeers] developed by
[RocketWerkz][rocketwerkz].

It uses keyword and regex matching to mimic the highlighting done by the game's
builtin IC editor.

_Note_: Syntax checking is not part of this plugin.

Configuration
-------------

No configuration is required. The default will mimic the highlighting from the
game's editor. However, extended highlighting is available by setting
`g:ic10_extended_syntax` to a truthy value. This will enable highlighting of
special words in comments (TODO, FIXME, XXX, NB and NOTE) as well as device and
register aliases.

Device and register alias highlighting can be disabled independently by
setting the corresponding global variable to a truthy value.

 * To disable _device_ aliases: `g:ic10_ext_alias_nodev`
 * To disable _register_ aliases: `g:ic10_ext_alias_noreg`

Vim example configuration:

    let g:ic10_extended_syntax = 1
    let g:ic10_ext_alias_nodev = 0
    let g:ic10_ext_alias_noreg = 1

Neovim example configuration:

    vim.g.ic10_extended_syntax = 1
    vim.g:ic10_ext_alias_nodev = 0
    vim.g:ic10_ext_alias_noreg = 1

[neovim]:      https://neovim.io
[rocketwerkz]: https://rocketwerkz.com
[stationeers]: https://store.steampowered.com/app/544550/Stationeers
[vim]:         https://www.vim.org
