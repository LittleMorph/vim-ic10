" Vim syntax file
" Language:    IC10
" Maintainer:  Kennet Reinhard
" Filenames:   *.ic10

if exists("b:current_syntax")
    finish
endif

setlocal iskeyword+=_
syntax case match

" Comments
if exists("g:ic10_extended_syntax") && g:ic10_extended_syntax
    syntax match ic10Comment "#.*" contains=ic10Todo
    syntax keyword ic10Todo contained TODO FIXME XXX NB NOTE
else
    syntax match ic10Comment "#.*"
endif

" Labels
syntax match ic10Label "\v^\w+\:"

" Numbers
syntax match ic10Number "\v<\d+>"
syntax match ic10Number "\v<\d+\.\d+>"
syntax match ic10Number "\v\$[0-9a-fA-F_]+>"
syntax match ic10Number "\v\%[01_]+>"

" Numeric Constants
syntax keyword ic10NumberConstant deg2rad rad2deg pi
syntax keyword ic10NumberConstant epsilon nan
syntax keyword ic10NumberConstant pinf ninf

" Devices
syntax match ic10Device "\v<d[b0-5]>"
syntax match ic10Device "\v<dr\d>"
syntax match ic10Device "\v<dr1[0-5]>"

" Registers
syntax match ic10Register "\v<r\d>"
syntax match ic10Register "\v<r1[0-5]>"
syntax keyword ic10Register sp ra

" String hash constants
syntax region ic10HashString start=/\V\(\<HASH(\)\@<="/ skip=/\v\\./ end=/\V")\@=/

" Builtins
syntax keyword ic10Builtin HASH

" Functions
syntax keyword ic10Function abs
syntax keyword ic10Function acos
syntax keyword ic10Function add
syntax keyword ic10Function alias
syntax keyword ic10Function and
syntax keyword ic10Function asin
syntax keyword ic10Function atan
syntax keyword ic10Function atan2
syntax keyword ic10Function bap bapal brap
syntax keyword ic10Function bapz bapzal brapz
syntax keyword ic10Function bdns bdnsal brdns
syntax keyword ic10Function bdse bdseal brdse
syntax keyword ic10Function beq beqal breq
syntax keyword ic10Function beqz beqzal breqz
syntax keyword ic10Function bge bgeal brge
syntax keyword ic10Function bgez bgezal brgez
syntax keyword ic10Function bgt bgtal brgt
syntax keyword ic10Function bgtz bgtzal brgtz
syntax keyword ic10Function ble bleal brle
syntax keyword ic10Function blez blezal brlez
syntax keyword ic10Function blt bltal brlt
syntax keyword ic10Function bltz bltzal brltz
syntax keyword ic10Function bna bnaal brna
syntax keyword ic10Function bnaz bnazal brnaz
syntax keyword ic10Function bnan bnanal brnan
syntax keyword ic10Function bne bneal brne
syntax keyword ic10Function bnez bnezal brnez
syntax keyword ic10Function ceil
syntax keyword ic10Function cos
syntax keyword ic10Function define
syntax keyword ic10Function div
syntax keyword ic10Function exp
syntax keyword ic10Function floor
syntax keyword ic10Function hcf
syntax keyword ic10Function j jal jr
syntax keyword ic10Function l ld
syntax keyword ic10Function lb lbn
syntax keyword ic10Function lbs lbns
syntax keyword ic10Function log
syntax keyword ic10Function lr
syntax keyword ic10Function ls
syntax keyword ic10Function max
syntax keyword ic10Function min
syntax keyword ic10Function mod
syntax keyword ic10Function move
syntax keyword ic10Function mul
syntax keyword ic10Function nor
syntax keyword ic10Function not
syntax keyword ic10Function or
syntax keyword ic10Function peek poke pop push
syntax keyword ic10Function put putd get getd clr clrd
syntax keyword ic10Function rand
syntax keyword ic10Function round
syntax keyword ic10Function s sd
syntax keyword ic10Function sap sapz
syntax keyword ic10Function sb sbn
syntax keyword ic10Function sbs
syntax keyword ic10Function sdns
syntax keyword ic10Function sdse
syntax keyword ic10Function select
syntax keyword ic10Function seq seqz
syntax keyword ic10Function sge sgez
syntax keyword ic10Function sgt sgtz
syntax keyword ic10Function sin
syntax keyword ic10Function sla sll
syntax keyword ic10Function sle slez
syntax keyword ic10Function sleep
syntax keyword ic10Function slt sltz
syntax keyword ic10Function sna snaz
syntax keyword ic10Function snan snanz
syntax keyword ic10Function sne snez
syntax keyword ic10Function sra srl
syntax keyword ic10Function ss
syntax keyword ic10Function sqrt
syntax keyword ic10Function sub
syntax keyword ic10Function tan
syntax keyword ic10Function trunc
syntax keyword ic10Function xor
syntax keyword ic10Function yield

" BatchModes
syntax keyword ic10BatchMode Average
syntax keyword ic10BatchMode Sum
syntax keyword ic10BatchMode Minimum
syntax keyword ic10BatchMode Maximum

" ReagentMode
syntax keyword ic10ReagentMode Contents
syntax keyword ic10ReagentMode Required
syntax keyword ic10ReagentMode Recipe
syntax keyword ic10ReagentMode TotalContents

" Device Variables (LogicType)
syntax keyword ic10Variable Acceleration
syntax keyword ic10Variable Activate
syntax keyword ic10Variable AirRelease
syntax keyword ic10Variable AlignmentError
syntax keyword ic10Variable Altitude
syntax keyword ic10Variable Apex
syntax keyword ic10Variable AutoLand
syntax keyword ic10Variable AutoShutOff
syntax keyword ic10Variable BestContactFilter
syntax keyword ic10Variable Bpm
syntax keyword ic10Variable BurnTimeRemaining
syntax keyword ic10Variable CelestialHash
syntax keyword ic10Variable CelestialParentHash
syntax keyword ic10Variable Channel0
syntax keyword ic10Variable Channel1
syntax keyword ic10Variable Channel2
syntax keyword ic10Variable Channel3
syntax keyword ic10Variable Channel4
syntax keyword ic10Variable Channel5
syntax keyword ic10Variable Channel6
syntax keyword ic10Variable Channel7
syntax keyword ic10Variable Charge
syntax keyword ic10Variable Chart
syntax keyword ic10Variable ChartedNavPoints
syntax keyword ic10Variable ClearMemory
syntax keyword ic10Variable CollectableGoods
syntax keyword ic10Variable Color
syntax keyword ic10Variable Combustion
syntax keyword ic10Variable CombustionInput
syntax keyword ic10Variable CombustionInput2
syntax keyword ic10Variable CombustionLimiter
syntax keyword ic10Variable CombustionOutput
syntax keyword ic10Variable CombustionOutput2
syntax keyword ic10Variable CompletionRatio
syntax keyword ic10Variable ContactTypeId
syntax keyword ic10Variable CurrentCode
syntax keyword ic10Variable CurrentResearchPodType
syntax keyword ic10Variable Density
syntax keyword ic10Variable DestinationCode
syntax keyword ic10Variable Discover
syntax keyword ic10Variable DistanceAu
syntax keyword ic10Variable DistanceKm
syntax keyword ic10Variable DrillCondition
syntax keyword ic10Variable DryMass
syntax keyword ic10Variable Eccentricity
syntax keyword ic10Variable ElevatorLevel
syntax keyword ic10Variable ElevatorSpeed
syntax keyword ic10Variable EntityState
syntax keyword ic10Variable EnvironmentEfficiency
syntax keyword ic10Variable Error
syntax keyword ic10Variable ExhaustVelocity
syntax keyword ic10Variable ExportCount
syntax keyword ic10Variable Extended
syntax keyword ic10Variable Filtration
syntax keyword ic10Variable FlightControlRule
syntax keyword ic10Variable Flush
syntax keyword ic10Variable ForceWrite
syntax keyword ic10Variable ForwardX
syntax keyword ic10Variable ForwardY
syntax keyword ic10Variable ForwardZ
syntax keyword ic10Variable Fuel
syntax keyword ic10Variable Harvest
syntax keyword ic10Variable Horizontal
syntax keyword ic10Variable HorizontalRatio
syntax keyword ic10Variable Idle
syntax keyword ic10Variable ImportCount
syntax keyword ic10Variable Inclination
syntax keyword ic10Variable Index
syntax keyword ic10Variable InterrogationProgress
syntax keyword ic10Variable LineNumber
syntax keyword ic10Variable Lock
syntax keyword ic10Variable ManualResearchRequiredPod
syntax keyword ic10Variable Mass
syntax keyword ic10Variable Maximum
syntax keyword ic10Variable MineablesInQueue
syntax keyword ic10Variable MineablesInVicinity
syntax keyword ic10Variable MinedQuantity
syntax keyword ic10Variable MinimumWattsToContact
syntax keyword ic10Variable Mode
syntax keyword ic10Variable NameHash
syntax keyword ic10Variable NavPoints
syntax keyword ic10Variable NextWeatherEventTime
syntax keyword ic10Variable On
syntax keyword ic10Variable Open
syntax keyword ic10Variable OperationalTemperatureEfficiency
syntax keyword ic10Variable OrbitPeriod
syntax keyword ic10Variable Orientation
syntax keyword ic10Variable Output
syntax keyword ic10Variable PassedMoles
syntax keyword ic10Variable Plant
syntax keyword ic10Variable PositionX
syntax keyword ic10Variable PositionY
syntax keyword ic10Variable PositionZ
syntax keyword ic10Variable Power
syntax keyword ic10Variable PowerActual
syntax keyword ic10Variable PowerGeneration
syntax keyword ic10Variable PowerPotential
syntax keyword ic10Variable PowerRequired
syntax keyword ic10Variable PrefabHash
syntax keyword ic10Variable Pressure
syntax keyword ic10Variable PressureEfficiency
syntax keyword ic10Variable PressureExternal
syntax keyword ic10Variable PressureInput
syntax keyword ic10Variable PressureInput2
syntax keyword ic10Variable PressureInternal
syntax keyword ic10Variable PressureOutput
syntax keyword ic10Variable PressureOuput2
syntax keyword ic10Variable PressureSetting
syntax keyword ic10Variable Progress
syntax keyword ic10Variable Quantity
syntax keyword ic10Variable Ratio
syntax keyword ic10Variable RatioCarbonDioxide
syntax keyword ic10Variable RatioCarbonDioxideInput
syntax keyword ic10Variable RatioCarbonDioxideInput2
syntax keyword ic10Variable RatioCarbonDioxideOutput
syntax keyword ic10Variable RatioCarbonDioxideOutput2
syntax keyword ic10Variable RatioHydrogen
syntax keyword ic10Variable RatioLiquidCarbonDioxide
syntax keyword ic10Variable RatioLiquidCarbonDioxideInput
syntax keyword ic10Variable RatioLiquidCarbonDioxideInput2
syntax keyword ic10Variable RatioLiquidCarbonDioxideOutput
syntax keyword ic10Variable RatioLiquidCarbonDioxideOutput2
syntax keyword ic10Variable RatioLiquidHydrogen
syntax keyword ic10Variable RatioLiquidNitrogen
syntax keyword ic10Variable RatioLiquidNitrogenInput
syntax keyword ic10Variable RatioLiquidNitrogenInput2
syntax keyword ic10Variable RatioLiquidNitrogenOutput
syntax keyword ic10Variable RatioLiquidNitrogenOutput2
syntax keyword ic10Variable RatioLiquidNitrousOxide
syntax keyword ic10Variable RatioLiquidNitrousOxideInput
syntax keyword ic10Variable RatioLiquidNitrousOxideInput2
syntax keyword ic10Variable RatioLiquidNitrousOxideOutput
syntax keyword ic10Variable RatioLiquidNitrousOxideOutput2
syntax keyword ic10Variable RatioLiquidOxygen
syntax keyword ic10Variable RatioLiquidOxygenInput
syntax keyword ic10Variable RatioLiquidOxygenInput2
syntax keyword ic10Variable RatioLiquidOxygenOutput
syntax keyword ic10Variable RatioLiquidOxygenOutput2
syntax keyword ic10Variable RatioLiquidPollutant
syntax keyword ic10Variable RatioLiquidPollutantInput
syntax keyword ic10Variable RatioLiquidPollutantInput2
syntax keyword ic10Variable RatioLiquidPollutantOutput
syntax keyword ic10Variable RatioLiquidPollutantOutput2
syntax keyword ic10Variable RatioLiquidVolatiles
syntax keyword ic10Variable RatioLiquidVolatilesInput
syntax keyword ic10Variable RatioLiquidVolatilesInput2
syntax keyword ic10Variable RatioLiquidVolatilesOutput
syntax keyword ic10Variable RatioLiquidVolatilesOutput2
syntax keyword ic10Variable RatioNitrogen
syntax keyword ic10Variable RatioNitrogenInput
syntax keyword ic10Variable RatioNitrogenInput2
syntax keyword ic10Variable RatioNitrogenOutput
syntax keyword ic10Variable RatioNitrogenOutput2
syntax keyword ic10Variable RatioNitrousOxide
syntax keyword ic10Variable RatioNitrousOxideInput
syntax keyword ic10Variable RatioNitrousOxideInput2
syntax keyword ic10Variable RatioNitrousOxideOutput
syntax keyword ic10Variable RatioNitrousOxideOutput2
syntax keyword ic10Variable RatioOxygen
syntax keyword ic10Variable RatioOxygenInput
syntax keyword ic10Variable RatioOxygenInput2
syntax keyword ic10Variable RatioOxygenOutput
syntax keyword ic10Variable RatioOxygenOutput2
syntax keyword ic10Variable RatioPollutant
syntax keyword ic10Variable RatioPollutantInput
syntax keyword ic10Variable RatioPollutantInput2
syntax keyword ic10Variable RatioPollutantOutput
syntax keyword ic10Variable RatioPollutantOutput2
syntax keyword ic10Variable RatioPollutedWater
syntax keyword ic10Variable RatioSteam
syntax keyword ic10Variable RatioSteamInput
syntax keyword ic10Variable RatioSteamInput2
syntax keyword ic10Variable RatioSteamOutput
syntax keyword ic10Variable RatioSteamOutput2
syntax keyword ic10Variable RatioVolatiles
syntax keyword ic10Variable RatioVolatilesInput
syntax keyword ic10Variable RatioVolatilesInput2
syntax keyword ic10Variable RatioVolatilesOutput
syntax keyword ic10Variable RatioVolatilesOutput2
syntax keyword ic10Variable RatioWater
syntax keyword ic10Variable RatioWaterInput
syntax keyword ic10Variable RatioWaterInput2
syntax keyword ic10Variable RatioWaterOutput
syntax keyword ic10Variable RatioWaterOutput2
syntax keyword ic10Variable Reagents
syntax keyword ic10Variable RecipeHash
syntax keyword ic10Variable ReEntryAltitude
syntax keyword ic10Variable ReferenceId
syntax keyword ic10Variable RequestHash
syntax keyword ic10Variable RequiredPower
syntax keyword ic10Variable ReturnFuelCost
syntax keyword ic10Variable Richness
syntax keyword ic10Variable Rpm
syntax keyword ic10Variable SimiMajorAxis
syntax keyword ic10Variable Setting
syntax keyword ic10Variable SettingInput
syntax keyword ic10Variable SettingOutput
syntax keyword ic10Variable SignalID
syntax keyword ic10Variable SignalStrength
syntax keyword ic10Variable Sites
syntax keyword ic10Variable Size
syntax keyword ic10Variable SizeX
syntax keyword ic10Variable SizeY
syntax keyword ic10Variable SizeZ
syntax keyword ic10Variable SolarAngel
syntax keyword ic10Variable Solarradiance
syntax keyword ic10Variable SoundAlert
syntax keyword ic10Variable Stress
syntax keyword ic10Variable Survey
syntax keyword ic10Variable TargetPadIndex
syntax keyword ic10Variable TargetPrefabHash
syntax keyword ic10Variable TargetSlotIndex
syntax keyword ic10Variable TargetX
syntax keyword ic10Variable TargetY
syntax keyword ic10Variable TargetZ
syntax keyword ic10Variable Temperature
syntax keyword ic10Variable TemperatureDifferentialEfficiency
syntax keyword ic10Variable TemperatureExternal
syntax keyword ic10Variable TemperatureInput
syntax keyword ic10Variable TemperatureInput2
syntax keyword ic10Variable TemperatureOutput
syntax keyword ic10Variable TemperatureOutput2
syntax keyword ic10Variable TemperatureSetting
syntax keyword ic10Variable Throttle
syntax keyword ic10Variable Thrust
syntax keyword ic10Variable ThrustToWeight
syntax keyword ic10Variable Time
syntax keyword ic10Variable TimeToDestination
syntax keyword ic10Variable TotalMoles
syntax keyword ic10Variable TotalMolesInput
syntax keyword ic10Variable TotalMolesInput2
syntax keyword ic10Variable TotalMolesOutput
syntax keyword ic10Variable TotalMolesOutput2
syntax keyword ic10Variable TotalQuantity
syntax keyword ic10Variable TrueAnomaly
syntax keyword ic10Variable VelocityMagnitude
syntax keyword ic10Variable VelocityRelativeX
syntax keyword ic10Variable VelocityRelativeY
syntax keyword ic10Variable VelocityRelativeZ
syntax keyword ic10Variable VelocityX
syntax keyword ic10Variable VelocityY
syntax keyword ic10Variable VelocityZ
syntax keyword ic10Variable Vertical
syntax keyword ic10Variable VerticalRatio
syntax keyword ic10Variable Volume
syntax keyword ic10Variable VolumeOfLiquid
syntax keyword ic10Variable WattsReachingContact
syntax keyword ic10Variable Weight
syntax keyword ic10Variable WorkingGasEfficiency

" Slot Variables (LogicSlotType)
syntax keyword ic10SlotVariable Charge
syntax keyword ic10SlotVariable ChargeRatio
syntax keyword ic10SlotVariable Class
syntax keyword ic10SlotVariable Damage
syntax keyword ic10SlotVariable Efficiency
syntax keyword ic10SlotVariable FilterType
syntax keyword ic10SlotVariable Growth
syntax keyword ic10SlotVariable HarvestedHash
syntax keyword ic10SlotVariable Health
syntax keyword ic10SlotVariable LineNumber
syntax keyword ic10SlotVariable Lock
syntax keyword ic10SlotVariable Mature
syntax keyword ic10SlotVariable MaxQuantity
syntax keyword ic10SlotVariable None
syntax keyword ic10SlotVariable OccupantHash
syntax keyword ic10SlotVariable Occupied
syntax keyword ic10SlotVariable On
syntax keyword ic10SlotVariable Open
syntax keyword ic10SlotVariable PrefabHash
syntax keyword ic10SlotVariable Pressure
syntax keyword ic10SlotVariable PressureAir
syntax keyword ic10SlotVariable PressureWaste
syntax keyword ic10SlotVariable Quantity
syntax keyword ic10SlotVariable ReferenceId
syntax keyword ic10SlotVariable Seeding
syntax keyword ic10SlotVariable SortingClass
syntax keyword ic10SlotVariable Temperature
syntax keyword ic10SlotVariable Volume

let b:current_syntax = "ic10"

highlight default link ic10Comment        Comment
highlight default link ic10Number         Number
highlight default link ic10NumberConstant Constant
highlight default link ic10Device         PreProc
highlight default link ic10Register       Type
highlight default link ic10Variable       Special
highlight default link ic10HashString     String
highlight default link ic10SlotVariable   Special
highlight default link ic10BatchMode      Special
highlight default link ic10ReagentMode    Special
highlight default link ic10Builtin        Keyword
highlight default link ic10Function       Function
highlight default link ic10Label          Label

if exists("g:ic10_extended_syntax") && g:ic10_extended_syntax
    highlight default link ic10Todo         Todo
endif
